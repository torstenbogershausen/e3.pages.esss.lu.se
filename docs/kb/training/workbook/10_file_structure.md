# Understanding e3's file structure

## Lesson Overview

In this lesson, you'll learn how to do the following:

* Understand all directories within e3.
* Know how to use `cmds/`.
* Know how to use `patch/`.
* Know how to use `template/`.
* Understand hidden files.
* Know how to structure an e3 IOC.

---

## e3 directory structure

To be written.

<!-- cmds/ is only for example start up scripts, not functional ones -->

<!-- files in patch/ should follow structure, see README and examples -->

<!-- template/ should include subst, template, and db files ready for use; add example in IOC startup script on how to include these properly -->

<!-- add minimalistic IOC example so people see that they shouldn't use e3 wrappers for this -->

