# Training

```{admonition} Under Construction
:class: warning

This workbook is a work-in-progress and is therefore still missing information. Please create an issue or submit a pull request with a suggested change if you noticy something being awry or have an improvement in mind.

Please also note that some chapters still have to be populated, and that the content of others already is out-of-date (as e3 itself still is under development).
```

This is a training series for using ESS' EPICS environment [e3](https://gitlab.esss.lu.se/e3/e3). The series assumes at least familiarity with [EPICS](https://epics-controls.org), albeit you may be able to follow along regardless.

---

Note that this workbook primarily is intended for e3 app developers and integrators at ESS (and consequently also contains some site specific processes and standards).

For some of the chapters there are supplementary directories containing code and utilities, so it's recommended to clone <https://gitlab.esss.lu.se/e3/e3-training-material> prior to starting.

:::{tip}
It is recommended to instead follow the tutorial directly in <https://gitlab.esss.lu.se/e3/e3training>, while the content is being adapted to fit into this portal.
:::

---

```{toctree}
:caption: Table of Content
:maxdepth: 1
:glob:
workbook/1_*
workbook/2_*
workbook/3_*
workbook/4_*
workbook/5_*
workbook/6_*
workbook/7_*
workbook/8_*
workbook/9_*
workbook/13_*
```
